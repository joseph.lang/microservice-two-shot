from django.db import models


# Create your models here.

class LocationVO(models.Model):
    href = models.CharField(max_length=200)
    location_id = models.IntegerField(unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField(max_length=500)
    location = models.ForeignKey(
        LocationVO,
        on_delete=models.CASCADE,
        related_name='hats',
    )
