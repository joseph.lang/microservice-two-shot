from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import LocationVO, Hat




class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style',
        'color',
        'picture',
        'id',
        ]
    def get_extra_data(self, hat):
        if hat.location is not None:
            return {
                'location':{
                    'location_href': hat.location.href,
                    'location_id': hat.location.location_id,
                    'closet_name': hat.location.closet_name,
                    'section_number': hat.location.section_number,
                    'shelf_number': hat.location.shelf_number,
                    }
            }
        return None


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_hats(request):

    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"Hats": hats},
            encoder=HatListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(id=content['location'])
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatListEncoder, safe=False)


@require_http_methods(["DELETE"])
def api_delete_hat(request, id):
    try:
        hat = Hat.objects.get(pk=id)
        hat.delete()
        return JsonResponse({'message': 'Goodbye sweet hat....(Delete successful)'})
    except Hat.DoesNotExist:
        return JsonResponse({'message': 'Hat not found'}, status=404)
