import React, { useEffect, useState } from "react";

const CreateShoe = () => {
  const [formData, setFormData] = useState({
    manufacturer: "",
    model_name: "",
    color: "",
    picture_url: "",
    bin: "",
  });

  const [bins, setBins] = useState([]);

  const getBins = async () => {
    try {
      const binsUrl = "http://localhost:8100/api/bins/";

      const getBinsRequest = await fetch(binsUrl);

      if (getBinsRequest.ok) {
        const binsList = await getBinsRequest.json();
        setBins(binsList.bins);
      }
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    getBins();
  }, []);

  const handleChange = (event) => {
    const input = event.target.name;
    const value = event.target.value;

    setFormData({
      ...formData,
      [input]: value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8080/api/shoes/";

    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const postRequest = await fetch(url, fetchOptions);

    if (postRequest.ok) {
      setFormData({
        manufacturer: "",
        model_name: "",
        color: "",
        picture_url: "",
        bin: "",
      });
      console.log("good");
    }
  };
  return (
    <>
      <form className="mt-4" onSubmit={handleSubmit}>
        <h1>Create Shoes</h1>
        <div className="form-group">
          <label htmlFor="manufacturer">Manufacturer</label>
          <input
            type="text"
            className="form-control"
            placeholder="Manufacturer"
            onChange={handleChange}
            name="manufacturer"
            value={formData.manufacturer}
          />
        </div>
        <div className="form-group">
          <label htmlFor="model">Model</label>
          <input
            type="text"
            className="form-control"
            placeholder="Model"
            onChange={handleChange}
            name="model_name"
            value={formData.model_name}
          />
        </div>
        <div className="form-group">
          <label htmlFor="color">Color</label>
          <input
            type="text"
            className="form-control"
            placeholder="Color"
            onChange={handleChange}
            name="color"
            value={formData.color}
          />
        </div>
        <div className="form-group">
          <label htmlFor="picture_url">Picture Url</label>
          {formData.picture_url.length > 750 ? (
            <>
              <input
                type="url"
                className="form-control"
                placeholder="Picture Url"
                onChange={handleChange}
                value={formData.picture_url}
                name="picture_url"
              />
              <p className="text-danger">picture url is too long</p>
            </>
          ) : (
            <input
              type="url"
              className="form-control"
              placeholder="Picture Url"
              onChange={handleChange}
              value={formData.picture_url}
              name="picture_url"
            />
          )}
        </div>
        <div className="mt-0.8 mb-3">
          <label>Bin</label>
          <select
            className="form-select"
            aria-label="Default select example"
            onChange={handleChange}
            value={formData.bin}
            name="bin"
          >
            <option>Choose a bin</option>
            {bins &&
              bins.map((bin) => {
                return (
                  <option key={bin.href} value={bin.href}>
                    {bin.id}
                  </option>
                );
              })}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </>
  );
};

export default CreateShoe;
