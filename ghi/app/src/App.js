import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListShoes from "./ListShoes";
import CreateShoe from "./CreateShoe";
import ListHats from "./ListHats";
import HatForm from "./CreateHat";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ListShoes />} />
          <Route path="/shoes/create" element={<CreateShoe />} />
          <Route path="/hats" element={<ListHats />} />
          <Route path="/hats/create" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
