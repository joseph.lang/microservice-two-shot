import React, { useEffect, useState } from "react";

const ListShoes = () => {
  const [shoes, setShoes] = useState([]);

  const getShoes = async () => {
    const shoesUrl = "http://localhost:8080/api/shoes/";

    const shoesRequest = await fetch(shoesUrl);

    if (shoesRequest.ok) {
      const shoesData = await shoesRequest.json();
      setShoes(shoesData.shoes);
    }
  };

  useEffect(() => {
    getShoes();
  }, []);

  const handleDelete = async (shoeId) => {
    const url = `http://localhost:8080/api/shoes/delete/${shoeId}/`;

    const fetchOptions = {
      method: "DELETE",
    };

    const deleteRequest = await fetch(url, fetchOptions);

    console.log(deleteRequest);

    if (deleteRequest.ok) {
      getShoes();
    }
  };
  return (
    <>
      <h1>List Shoes</h1>
      <table className="table table-striped mt-4">
        <tbody>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Picture</th>
            <th></th>
          </tr>
          {shoes &&
            shoes.map((shoe) => (
              <tr key={shoe.id} value={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>
                  <img
                    src={shoe.picture_url}
                    style={{
                      height: "4.5rem",
                      width: "4.5rem",
                      borderRadius: "0.3rem",
                    }}
                  />
                </td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => handleDelete(shoe.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
};

export default ListShoes;
