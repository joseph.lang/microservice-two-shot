import React, {useEffect, useState} from 'react';


function HatForm () {

    const [fabric,setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    };

    const[style,setStyle] = useState('');
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    };

    const[color,setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const[picture,setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    };

    const[location,setLocation] = useState(0);
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    };

    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        };
      };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.picture = picture;
        data.location = location; //need to make this location hook equal to the id to match backend post

        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            setFabric('');
            setStyle('');
            setColor('');
            setPicture('');
            setLocation('');
            event.target.reset();
        };
    };

      useEffect(() => {
        fetchData();
      }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form
            id="create-hat-form"
            onSubmit = {handleSubmit}
            >
              <div className="form-floating mb-3">
                <input
                onChange={handleFabricChange}
                value={fabric}
                placeholder="Name"
                required type="text"
                name="name"
                id="name"
                className="form-control"
                />
                <label htmlFor="name">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleStyleChange}
                value={style}
                placeholder="Room count"
                required type="text"
                name="room_count"
                id="room_count"
                className="form-control"
                />
                <label htmlFor="room_count">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleColorChange}
                value={color}
                placeholder="City"
                required type="text"
                name="city"
                id="city"
                className="form-control"
                />
                <label htmlFor="city">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handlePictureChange}
                value={picture}
                placeholder="City"
                required type="url"
                name="city"
                id="city"
                className="form-control"
                />
                <label htmlFor="city">Picture URL</label>
              </div>
              <div className="mb-3">
                <select
                onChange={handleLocationChange}
                value={location}
                required name="state"
                id="state"
                className="form-select"
                >
                  <option  value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key = {location['id']} value = {location['id']}>
                            {location['closet_name']}
                        </option>
                    )
                  }) }
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}






export default HatForm;
