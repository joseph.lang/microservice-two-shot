import React, { useState, useEffect } from 'react';

function ListHats() {

    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const getURL = 'http://localhost:8090/api/hats';
        const response = await fetch(getURL);
        if (response.ok) {
          const data = await response.json();
          setHats(data.Hats);
        };
      };

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (id) => {
        const deleteURL = `http://localhost:8090/api/hats/${id}`;
        const response = await fetch(deleteURL, {
            method:'DELETE',
        });

        if (response.ok) {
            const updatedHats = hats.filter((hat) => hat.id !== id);
            setHats(updatedHats);
        }
    }

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => (
                    <tr key={hat.id}>
                        <td>{hat.fabric}</td>
                        <td>{hat.style}</td>
                        <td>{hat.color}</td>
                        <td>
                            <img src={hat.picture} alt={`${hat.style} hat`} style={{width: "100px"}} />
                        </td>
                        <td>
                            <button onClick = {() => handleDelete(hat.id)}>Delete</button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ListHats;
