# Wardrobify

Team:

- Person 1 - Hats, Jonas Zara
- Person 2 - Shoes, Joseph Lang

## Design

## Shoes microservice

The Shoes microservice is comprised of two models which include BinVO and Shoe. BinVO is a value object that gets updated from a polling function that retrieves data from the wardrobe microservice and gives BinVO a reference of that data to use as a copy. I created create shoes, list shoes, and delete shoes functions in the backend and routed them to create the api endpoints. I then created the react components so users can perform the same operations on the frontend.

## Hats microservice

The Hats microservice will use a polling script to retrieve location data from the Wardrobe API. This data will be mirrored to the LocationVO model created within the Hats microservice which acts as the primary key to the Hat model's location field. This way, the hats and wardrobes the hats belong to, can be separate yet connected within the project allowing the hats microservice to focus on hats, and the wardrobes microservice to focus on wardrobes.
