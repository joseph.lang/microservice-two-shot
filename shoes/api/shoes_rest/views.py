from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import BinVO, Shoe
# Create your views here.

class ShoeListEncoder(ModelEncoder):
  model = Shoe
  properties = [ 'manufacturer', 'model_name', 'color', 'picture_url', 'id' ]

  def get_extra_data(self, o):
    return {"bin": o.bin.bin_href}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
  if request.method == "GET":
      shoes = Shoe.objects.all()
      return JsonResponse (
        {'shoes': shoes},
        encoder=ShoeListEncoder
      )
  else:
    try:
      new_shoes_data = json.loads(request.body)
      bin_href = new_shoes_data['bin']
      bin = BinVO.objects.get(bin_href=bin_href)
      new_shoes_data['bin'] = bin
    except BinVO.DoesNotExist:
      return JsonResponse(
        "Bin href does not exist",
        status = 404,
        safe=False,
      )
    new_shoes = Shoe.objects.create(**new_shoes_data)
    return JsonResponse(
      new_shoes,
      encoder=ShoeListEncoder,
      safe=False
      )
@require_http_methods(['DELETE'])
def api_delete_shoes(request, id):
  try:
    shoes = Shoe.objects.get(id=id)
    shoes.delete()
  except Shoe.DoesNotExist:
    return JsonResponse(
      'Shoes id does not exist',
      status=404,
      safe=False
    )
  return JsonResponse(
    'Shoes deleted successfully',
    encoder=ShoeListEncoder,
    safe=False
  )
