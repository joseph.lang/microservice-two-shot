from django.db import models

# Create your models here.

class BinVO(models.Model):
  closet_name = models.CharField(max_length=200)
  bin_href = models.CharField(max_length=200, null=True)
  bin_number = models.PositiveSmallIntegerField(null=True)

  def __str__(self):
    return f'{self.id}'

class Shoe(models.Model):
  manufacturer = models.CharField(max_length=200)
  model_name = models.CharField(max_length=200)
  color = models.CharField(max_length=200)
  picture_url = models.URLField(max_length=770)
  bin = models.ForeignKey(BinVO, on_delete=models.CASCADE)
