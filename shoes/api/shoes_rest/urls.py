from .views import api_list_shoes, api_delete_shoes
from django.urls import path
urlpatterns = [
  path('shoes/', api_list_shoes, name="api_list_shoes"),
  path('shoes/delete/<int:id>/', api_delete_shoes, name="api_delete_shoes")
]
