import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO

def get_bins():
    get_bins_request =requests.get("http://wardrobe-api:8000/api/bins/")
    bins = get_bins_request.json()
    for bin in bins["bins"]:
        BinVO.objects.update_or_create(
            bin_href = bin['href'],
            closet_name = bin['closet_name'],
            bin_number = bin['bin_number']
        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bins()
            print('success')
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
